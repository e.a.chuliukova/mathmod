import QtQuick
import QtQuick3D

Node {
    // Materials
    PrincipledMaterial {
        id: iSS_03_dull_material
        baseColorMap: Texture {
            source: "maps/ISS_03.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        roughness: 0.75
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_AO_06_material
        baseColorMap: Texture {
            source: "maps/ISS_AO_06.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 0.5
        roughness: 0.45
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_01_dull_material
        baseColorMap: Texture {
            source: "maps/ISS_01.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 0.25
        roughness: 0.5
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_02_dull_material
        baseColorMap: Texture {
            source: "maps/ISS_02.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 0.5
        roughness: 0.6
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: shiny_panel_material
        baseColorMap: Texture {
            source: "maps/ISS_02.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 0.25
        roughness: 0.16
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: olive___material
        baseColor: "#ff3d3a29"
        metalness: 1
        roughness: 0.3
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_AO_02_material
        baseColorMap: Texture {
            source: "maps/ISS_AO_02.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 1
        roughness: 0.45
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_AO_04_material
        baseColorMap: Texture {
            source: "maps/ISS_AO_04.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 0.5
        roughness: 0.45
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_AO_01_material
        baseColorMap: Texture {
            source: "maps/ISS_AO_01.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 0.5
        roughness: 0.45
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_AO_03_material
        baseColorMap: Texture {
            source: "maps/ISS_AO_03.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 0.5
        roughness: 0.45
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_01_shiny_n_material
        baseColorMap: Texture {
            source: "maps/ISS_01.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 0.5
        roughness: 0.45
        normalMap: Texture {
            source: "maps/ISS_01_n.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        normalStrength: 0.35
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_01_dark___material
        baseColorMap: Texture {
            source: "maps/ISS_01.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        roughness: 0.5
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: foil_silver_material
        metalness: 1
        roughness: 0.3
        normalMap: Texture {
            source: "maps/foil_n.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        normalStrength: 2
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_AO_05_material
        baseColorMap: Texture {
            source: "maps/ISS_AO_05.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 0.5
        roughness: 0.45
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_04_dull_material
        baseColorMap: Texture {
            source: "maps/ISS_04.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        roughness: 0.5
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_02_dark___material
        baseColorMap: Texture {
            source: "maps/ISS_02.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        roughness: 0.5
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_AO_07_material
        baseColorMap: Texture {
            source: "maps/ISS_AO_07.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 0.5
        roughness: 0.45
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_AO_08_material
        baseColorMap: Texture {
            source: "maps/ISS_AO_08.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 0.5
        roughness: 0.45
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_03_shiny_n_material
        baseColorMap: Texture {
            source: "maps/ISS_03.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 0.5
        roughness: 0.45
        normalMap: Texture {
            source: "maps/ISS_03_n.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        normalStrength: 0.35
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: white_material
        baseColor: "#ff7f7f7f"
        roughness: 0.85
        normalMap: Texture {
            source: "maps/foil_n.png.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: base_metal_material
        baseColor: "#ff515041"
        metalness: 1
        roughness: 1
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_03_dull_001_material
        baseColorMap: Texture {
            source: "maps/ISS_03.png.002.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 1
        roughness: 0.673268
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: ecostress_material
        baseColorMap: Texture {
            source: "maps/ecostress.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        roughness: 0.75
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: plastic_black_material
        baseColor: "#ff070707"
        roughness: 0.5
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: ecostressWhite_material
        roughness: 0.5
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: iSS_03_dull_002_material
        baseColorMap: Texture {
            source: "maps/ISS_03.jpg.001.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        roughness: 0.75
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: ecostress_metal_material
        baseColorMap: Texture {
            source: "maps/ecostress.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        metalness: 0.6
        roughness: 0.75
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: ecostress_dexter_material
        baseColorMap: Texture {
            source: "maps/ISS_01.jpg.png"
            generateMipmaps: true
            mipFilter: Texture.Linear
        }
        opacityChannel: Material.A
        roughness: 0.5
        alphaMode: PrincipledMaterial.Opaque
    }
    PrincipledMaterial {
        id: _material
        metalness: 1
        roughness: 1
        alphaMode: PrincipledMaterial.Opaque
    }
    // end of Materials

    Node {
        id: rOOT
        Node {
            id: _root
            Model {
                id: node02_Unity_Node_1
                source: "meshes/node02_Unity_Node_1.mesh"
                materials: [
                    _material,
                    iSS_01_dull_material,
                    iSS_01_shiny_n_material,
                    iSS_AO_01_material
                ]
                Model {
                    id: node03__PMA__Pressurized_Mating_Adapter_1
                    z: -3.38345
                    source: "meshes/node03__PMA__Pressurized_Mating_Adapter_1.mesh"
                    materials: [
                        iSS_AO_02_material,
                        iSS_AO_01_material,
                        iSS_01_dull_material
                    ]
                    Model {
                        id: node01_Zarya____FGB__Funtional_Cargo_Block
                        y: 0.734777
                        z: -2.45749
                        source: "meshes/node01_Zarya____FGB__Funtional_Cargo_Block.mesh"
                        materials: [
                            _material,
                            iSS_02_dull_material,
                            iSS_01_dull_material,
                            iSS_AO_02_material
                        ]
                        Model {
                            id: node05_Zvezda__SM__Service_Module
                            z: -12.9649
                            source: "meshes/node05_Zvezda__SM__Service_Module.mesh"
                            materials: [
                                _material,
                                iSS_03_dull_material,
                                iSS_02_dull_material,
                                iSS_01_dull_material,
                                iSS_AO_02_material
                            ]
                            Model {
                                id: node13_Pirs_Docking_Compartment__DC__and_Airlock
                                y: -1.15929
                                z: -1.15722
                                source: "meshes/node13_Pirs_Docking_Compartment__DC__and_Airlock.mesh"
                                materials: [
                                    iSS_03_dull_material,
                                    iSS_AO_06_material,
                                    iSS_01_dull_material,
                                    iSS_02_dull_material
                                ]
                            }
                            Model {
                                id: node34_Poisk__MRM_2__Mini_Research_Module
                                y: 1.15727
                                z: -1.15722
                                source: "meshes/node34_Poisk__MRM_2__Mini_Research_Module.mesh"
                                materials: [
                                    iSS_03_dull_material,
                                    iSS_AO_06_material,
                                    iSS_01_dull_material,
                                    iSS_02_dull_material
                                ]
                            }
                            Model {
                                id: panel_01_p_001
                                x: 2.22685
                                y: -0.03298
                                z: -5.25661
                                source: "meshes/panel_01_p_001.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_01_s_001
                                x: -2.22685
                                y: -0.0329794
                                z: -5.26025
                                source: "meshes/panel_01_s_001.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_02_p_001
                                x: 3.51119
                                y: -0.0307977
                                z: -5.25843
                                source: "meshes/panel_02_p_001.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_02_s_001
                                x: -3.51119
                                y: -0.0307978
                                z: -5.25846
                                source: "meshes/panel_02_s_001.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_03_p_001
                                x: 5.03229
                                y: -0.0307973
                                z: -5.25841
                                source: "meshes/panel_03_p_001.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_03_s_001
                                x: -5.0323
                                y: -0.0307986
                                z: -5.25846
                                source: "meshes/panel_03_s_001.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_04_p_001
                                x: 6.55613
                                y: -0.0307984
                                z: -5.25841
                                source: "meshes/panel_04_p_001.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_04_s_001
                                x: -6.55614
                                y: -0.0307977
                                z: -5.25846
                                source: "meshes/panel_04_s_001.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_05_p
                                x: 8.06586
                                y: -0.0307986
                                z: -5.25841
                                source: "meshes/panel_05_p.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_05_s
                                x: -8.06586
                                y: -0.0307981
                                z: -5.25846
                                source: "meshes/panel_05_s.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_06_p
                                x: 9.58453
                                y: -0.0307984
                                z: -5.25842
                                source: "meshes/panel_06_p.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_06_s
                                x: -9.58453
                                y: -0.0307976
                                z: -5.25846
                                source: "meshes/panel_06_s.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_07_p
                                x: 11.1616
                                y: -0.0307984
                                z: -5.25841
                                source: "meshes/panel_07_p.mesh"
                                materials: [
                                    iSS_02_dull_material,
                                    shiny_panel_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_07_s
                                x: -11.1616
                                y: -0.0307983
                                z: -5.25846
                                source: "meshes/panel_07_s.mesh"
                                materials: [
                                    iSS_02_dull_material,
                                    shiny_panel_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_08_p
                                x: 12.7426
                                y: -0.0307989
                                z: -5.25842
                                source: "meshes/panel_08_p.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_08_s
                                x: -12.7426
                                y: -0.0307988
                                z: -5.25846
                                source: "meshes/panel_08_s.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_09_p
                                x: 14.3276
                                y: -0.0307992
                                z: -5.25842
                                source: "meshes/panel_09_p.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_09_s
                                x: -14.3276
                                y: -0.0307996
                                z: -5.25846
                                source: "meshes/panel_09_s.mesh"
                                materials: [
                                    shiny_panel_material,
                                    iSS_02_dull_material,
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_bracket_p
                                x: 1.89571
                                y: -0.0329795
                                z: -5.25844
                                source: "meshes/panel_bracket_p.mesh"
                                materials: [
                                    olive___material
                                ]
                            }
                            Model {
                                id: panel_bracket_s
                                x: -1.89571
                                y: -0.0329801
                                z: -5.25843
                                source: "meshes/panel_bracket_s.mesh"
                                materials: [
                                    olive___material
                                ]
                            }
                        }
                        Model {
                            id: node39_Rassvet__MRM_1__Mini_Research_Module
                            y: -4.58714
                            z: -1.15393
                            source: "meshes/node39_Rassvet__MRM_1__Mini_Research_Module.mesh"
                            materials: [
                                iSS_01_dull_material,
                                iSS_03_dull_material,
                                iSS_02_dull_material,
                                iSS_AO_04_material
                            ]
                        }
                    }
                }
                Model {
                    id: node06_Z1_Truss
                    y: 1.75916
                    source: "meshes/node06_Z1_Truss.mesh"
                    materials: [
                        iSS_01_dull_material,
                        iSS_AO_03_material,
                        iSS_01_shiny_n_material,
                        iSS_01_dark___material,
                        foil_silver_material
                    ]
                }
                Model {
                    id: node09_Destiny_Space_Laboratory
                    z: 2.22104
                    source: "meshes/node09_Destiny_Space_Laboratory.mesh"
                    materials: [
                        _material,
                        iSS_AO_03_material,
                        iSS_01_dull_material,
                        iSS_AO_01_material,
                        iSS_01_shiny_n_material,
                        iSS_03_shiny_n_material
                    ]
                    Model {
                        id: node10__ESP__External_Stowage_Platform_1
                        x: 2.72939
                        y: -0.476999
                        z: 2.00751
                        source: "meshes/node10__ESP__External_Stowage_Platform_1.mesh"
                        materials: [
                            iSS_01_dull_material,
                            iSS_03_dull_material
                        ]
                    }
                    Model {
                        id: node14_S0_Truss
                        y: 5.00707
                        z: 0.966509
                        source: "meshes/node14_S0_Truss.mesh"
                        materials: [
                            iSS_02_dull_material,
                            iSS_02_dark___material,
                            iSS_01_dull_material,
                            iSS_AO_04_material,
                            iSS_04_dull_material
                        ]
                        Model {
                            id: node15__MBS__Mobile_Base_System
                            x: -2.86546
                            y: -0.595475
                            z: 5.17004
                            source: "meshes/node15__MBS__Mobile_Base_System.mesh"
                            materials: [
                                iSS_02_dull_material,
                                iSS_01_dull_material,
                                iSS_AO_05_material,
                                iSS_03_dull_material
                            ]
                            Model {
                                id: node11_Canadarm2
                                x: -1.43473
                                y: 1.59009
                                z: 0.198475
                                source: "meshes/node11_Canadarm2.mesh"
                                materials: [
                                    iSS_03_dull_material
                                ]
                                Model {
                                    id: node11_Canadarm2_02
                                    x: -0.669272
                                    y: 0.3556
                                    z: 0.979936
                                    source: "meshes/node11_Canadarm2_02.mesh"
                                    materials: [
                                        iSS_03_dull_material
                                    ]
                                    Model {
                                        id: node11_Canadarm2_03
                                        x: -0.00587416
                                        y: 0.358009
                                        z: 0.229634
                                        source: "meshes/node11_Canadarm2_03.mesh"
                                        materials: [
                                            iSS_03_dull_material
                                        ]
                                        Model {
                                            id: node11_Canadarm2_04
                                            x: 0.133868
                                            y: 0.253629
                                            z: 0.314147
                                            source: "meshes/node11_Canadarm2_04.mesh"
                                            materials: [
                                                iSS_03_dull_material
                                            ]
                                            Model {
                                                id: node11_Canadarm2_05
                                                x: -7.34353
                                                y: 0.00439167
                                                z: 0.575174
                                                source: "meshes/node11_Canadarm2_05.mesh"
                                                materials: [
                                                    iSS_03_dull_material
                                                ]
                                                Model {
                                                    id: node11_Canadarm2_06
                                                    x: 0.00439453
                                                    y: 7.34353
                                                    z: 0.575174
                                                    source: "meshes/node11_Canadarm2_06.mesh"
                                                    materials: [
                                                        iSS_03_dull_material
                                                    ]
                                                    Model {
                                                        id: node11_Canadarm2_07
                                                        x: -0.287583
                                                        y: 0.00219154
                                                        z: 0.313404
                                                        source: "meshes/node11_Canadarm2_07.mesh"
                                                        materials: [
                                                            iSS_03_dull_material
                                                        ]
                                                        Model {
                                                            id: node11_Canadarm2_08
                                                            x: -0.313413
                                                            y: 0.287587
                                                            z: 0.00219917
                                                            source: "meshes/node11_Canadarm2_08.mesh"
                                                            materials: [
                                                                iSS_03_dull_material
                                                            ]
                                                            Model {
                                                                id: node29_DEXTRE
                                                                x: 2.86102e-06
                                                                y: 1.20334
                                                                z: -2.86102e-06
                                                                source: "meshes/node29_DEXTRE.mesh"
                                                                materials: [
                                                                    iSS_03_dull_material,
                                                                    iSS_01_dull_material
                                                                ]
                                                                Model {
                                                                    id: node29_DEXTRE_001
                                                                    x: 3.8147e-06
                                                                    y: 0.22015
                                                                    z: -5.72205e-06
                                                                    source: "meshes/node29_DEXTRE_001.mesh"
                                                                    materials: [
                                                                        iSS_03_dull_material
                                                                    ]
                                                                    Model {
                                                                        id: node29_DEXTRE_002
                                                                        x: -2.86102e-06
                                                                        y: 1.96192
                                                                        z: 0.0059824
                                                                        source: "meshes/node29_DEXTRE_002.mesh"
                                                                        materials: [
                                                                            iSS_03_dull_material
                                                                        ]
                                                                        Model {
                                                                            id: node29_DEXTRE_003
                                                                            y: 0.962871
                                                                            source: "meshes/node29_DEXTRE_003.mesh"
                                                                            materials: [
                                                                                iSS_03_dull_material
                                                                            ]
                                                                        }
                                                                    }
                                                                    Model {
                                                                        id: node29_DEXTRE_004
                                                                        x: 1.37982
                                                                        y: 1.42507
                                                                        z: 0.000118256
                                                                        source: "meshes/node29_DEXTRE_004.mesh"
                                                                        materials: [
                                                                            iSS_03_dull_material
                                                                        ]
                                                                        Model {
                                                                            id: node29_DEXTRE_005
                                                                            x: 0.374912
                                                                            y: -0.103072
                                                                            z: 0.268671
                                                                            source: "meshes/node29_DEXTRE_005.mesh"
                                                                            materials: [
                                                                                iSS_03_dull_material
                                                                            ]
                                                                            Model {
                                                                                id: node29_DEXTRE_006
                                                                                x: 0.303666
                                                                                y: -0.11332
                                                                                z: 0.14278
                                                                                source: "meshes/node29_DEXTRE_006.mesh"
                                                                                materials: [
                                                                                    iSS_03_dull_material
                                                                                ]
                                                                                Model {
                                                                                    id: node29_DEXTRE_007
                                                                                    x: 1.08734
                                                                                    y: 0.0125704
                                                                                    z: 0.0614758
                                                                                    source: "meshes/node29_DEXTRE_007.mesh"
                                                                                    materials: [
                                                                                        iSS_03_dull_material
                                                                                    ]
                                                                                    Model {
                                                                                        id: node29_DEXTRE_008
                                                                                        x: 1.08734
                                                                                        y: -0.0125771
                                                                                        z: -0.327104
                                                                                        source: "meshes/node29_DEXTRE_008.mesh"
                                                                                        materials: [
                                                                                            iSS_03_dull_material
                                                                                        ]
                                                                                        Model {
                                                                                            id: node29_DEXTRE_009
                                                                                            x: 0.281783
                                                                                            y: -0.120009
                                                                                            z: -0.111829
                                                                                            source: "meshes/node29_DEXTRE_009.mesh"
                                                                                            materials: [
                                                                                                iSS_03_dull_material
                                                                                            ]
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    Model {
                                                                        id: node29_DEXTRE_010
                                                                        x: -1.37982
                                                                        y: 1.42507
                                                                        z: 0.000118256
                                                                        source: "meshes/node29_DEXTRE_010.mesh"
                                                                        materials: [
                                                                            iSS_03_dull_material
                                                                        ]
                                                                        Model {
                                                                            id: node29_DEXTRE_011
                                                                            x: -0.374923
                                                                            y: -0.103072
                                                                            z: 0.268673
                                                                            source: "meshes/node29_DEXTRE_011.mesh"
                                                                            materials: [
                                                                                iSS_03_dull_material
                                                                            ]
                                                                            Model {
                                                                                id: node29_DEXTRE_012
                                                                                x: -0.303657
                                                                                y: -0.11332
                                                                                z: 0.14278
                                                                                source: "meshes/node29_DEXTRE_012.mesh"
                                                                                materials: [
                                                                                    iSS_03_dull_material
                                                                                ]
                                                                                Model {
                                                                                    id: node29_DEXTRE_013
                                                                                    x: -1.08734
                                                                                    y: 0.0125767
                                                                                    z: 0.0614719
                                                                                    source: "meshes/node29_DEXTRE_013.mesh"
                                                                                    materials: [
                                                                                        iSS_03_dull_material
                                                                                    ]
                                                                                    Model {
                                                                                        id: node29_DEXTRE_014
                                                                                        x: -1.08734
                                                                                        y: -0.0125811
                                                                                        z: -0.327102
                                                                                        source: "meshes/node29_DEXTRE_014.mesh"
                                                                                        materials: [
                                                                                            iSS_03_dull_material
                                                                                        ]
                                                                                        Model {
                                                                                            id: node29_DEXTRE_015
                                                                                            x: -0.281775
                                                                                            y: -0.120026
                                                                                            z: -0.111833
                                                                                            source: "meshes/node29_DEXTRE_015.mesh"
                                                                                            materials: [
                                                                                                iSS_03_dull_material
                                                                                            ]
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        Model {
                            id: node16_S1_Truss
                            x: -13.808
                            y: 7.34329e-05
                            z: 2.4374
                            source: "meshes/node16_S1_Truss.mesh"
                            materials: [
                                iSS_02_dull_material,
                                iSS_01_dull_material,
                                iSS_02_dark___material,
                                iSS_AO_05_material,
                                iSS_04_dull_material
                            ]
                            Model {
                                id: node16_S1_Truss_02
                                x: -1.44092
                                y: -0.00846624
                                z: -0.939924
                                source: "meshes/node16_S1_Truss_02.mesh"
                                materials: [
                                    iSS_04_dull_material,
                                    iSS_AO_05_material,
                                    iSS_01_dull_material,
                                    iSS_02_dull_material
                                ]
                            }
                            Model {
                                id: node22_S3_Truss
                                x: -10.0483
                                y: 0.00509501
                                z: -0.822398
                                source: "meshes/node22_S3_Truss.mesh"
                                materials: [
                                    iSS_01_dull_material,
                                    iSS_04_dull_material,
                                    iSS_AO_05_material
                                ]
                                Model {
                                    id: node23_S4_Truss
                                    x: -3.0228
                                    y: -0.026546
                                    z: -0.0131292
                                    rotation: Qt.quaternion(0.866025, 0.5, 0, 0)
                                    source: "meshes/node23_S4_Truss.mesh"
                                    materials: [
                                        iSS_AO_05_material,
                                        iSS_02_dull_material,
                                        iSS_AO_03_material,
                                        iSS_02_dark___material,
                                        iSS_01_dull_material
                                    ]
                                    Model {
                                        id: node24_S5_Truss
                                        x: -8.1391
                                        y: -0.00870419
                                        z: 1.23359
                                        source: "meshes/node24_S5_Truss.mesh"
                                        materials: [
                                            iSS_01_dull_material,
                                            iSS_AO_06_material,
                                            iSS_02_dull_material,
                                            iSS_02_dark___material
                                        ]
                                        Model {
                                            id: node23_S4_Truss_01
                                            x: 0.487255
                                            y: 0.684659
                                            z: 0.665065
                                            source: "meshes/node23_S4_Truss_01.mesh"
                                            materials: [
                                                iSS_02_dull_material,
                                                shiny_panel_material,
                                                iSS_02_dark___material
                                            ]
                                        }
                                        Model {
                                            id: node23_S4_Truss_02
                                            x: 0.487251
                                            y: -0.680742
                                            z: -2.39077
                                            source: "meshes/node23_S4_Truss_02.mesh"
                                            materials: [
                                                iSS_02_dull_material,
                                                shiny_panel_material,
                                                iSS_02_dark___material
                                            ]
                                        }
                                        Model {
                                            id: node32_S6_Truss
                                            x: -1.44467
                                            z: -0.84866
                                            source: "meshes/node32_S6_Truss.mesh"
                                            materials: [
                                                iSS_02_dull_material,
                                                iSS_AO_03_material,
                                                iSS_01_dull_material,
                                                iSS_02_dark___material
                                            ]
                                            Model {
                                                id: node32_S6_Truss_01
                                                x: -13.4921
                                                y: 0.683402
                                                z: 1.52168
                                                source: "meshes/node32_S6_Truss_01.mesh"
                                                materials: [
                                                    iSS_02_dull_material,
                                                    shiny_panel_material,
                                                    iSS_02_dark___material
                                                ]
                                            }
                                            Model {
                                                id: node32_S6_Truss_02
                                                x: -13.4921
                                                y: -0.681999
                                                z: -1.53416
                                                source: "meshes/node32_S6_Truss_02.mesh"
                                                materials: [
                                                    iSS_02_dull_material,
                                                    shiny_panel_material,
                                                    iSS_02_dark___material
                                                ]
                                            }
                                        }
                                    }
                                }
                                Model {
                                    id: node25__ESP__External_Stowage_Platform_3
                                    x: -1.31253
                                    y: -2.24556
                                    z: -0.00264311
                                    source: "meshes/node25__ESP__External_Stowage_Platform_3.mesh"
                                    materials: [
                                        iSS_01_dull_material,
                                        iSS_01_shiny_n_material,
                                        iSS_AO_07_material,
                                        iSS_03_dull_material,
                                        iSS_04_dull_material
                                    ]
                                }
                                Model {
                                    id: node36_Express_Logistics_Carrier__ELC__2
                                    x: -1.84494
                                    y: 5.05736
                                    z: -0.00264263
                                    source: "meshes/node36_Express_Logistics_Carrier__ELC__2.mesh"
                                    materials: [
                                        iSS_AO_07_material,
                                        iSS_04_dull_material,
                                        iSS_01_dull_material,
                                        iSS_03_dull_material
                                    ]
                                }
                                Model {
                                    id: node41_Express_Logistics_Carrier__ELC__4
                                    x: 2.20817
                                    y: -5.07413
                                    z: -0.00264263
                                    source: "meshes/node41_Express_Logistics_Carrier__ELC__4.mesh"
                                    materials: [
                                        iSS_04_dull_material,
                                        iSS_01_dull_material,
                                        iSS_AO_07_material
                                    ]
                                }
                                Model {
                                    id: node42_Alpha_Magnetic_Spectrometer__AMS_2_
                                    x: 2.10872
                                    y: 3.71699
                                    z: -0.23629
                                    source: "meshes/node42_Alpha_Magnetic_Spectrometer__AMS_2_.mesh"
                                    materials: [
                                        iSS_04_dull_material,
                                        iSS_AO_07_material,
                                        iSS_01_dull_material
                                    ]
                                }
                            }
                            Model {
                                id: node44_Enhanced_ISS_Boom_Assembly__EIBA_
                                x: 2.68104
                                y: 1.90843
                                z: 0.125051
                                source: "meshes/node44_Enhanced_ISS_Boom_Assembly__EIBA_.mesh"
                                materials: [
                                    iSS_03_dull_material,
                                    iSS_AO_07_material,
                                    iSS_01_dull_material
                                ]
                            }
                        }
                        Model {
                            id: node17_P1_Truss
                            x: 6.82182
                            y: 2.23725
                            z: 1.50449
                            source: "meshes/node17_P1_Truss.mesh"
                            materials: [
                                iSS_02_dull_material,
                                iSS_01_dull_material,
                                iSS_02_dark___material,
                                iSS_AO_05_material,
                                iSS_04_dull_material
                            ]
                            Model {
                                id: node17_P1_Truss_02
                                x: 8.42707
                                y: -2.23084
                                z: -0.0069766
                                source: "meshes/node17_P1_Truss_02.mesh"
                                materials: [
                                    iSS_04_dull_material,
                                    iSS_AO_05_material,
                                    iSS_01_dull_material,
                                    iSS_02_dull_material
                                ]
                            }
                            Model {
                                id: node19_P3_Truss
                                x: 17.0416
                                y: -2.23208
                                z: 0.110505
                                source: "meshes/node19_P3_Truss.mesh"
                                materials: [
                                    iSS_01_dull_material,
                                    iSS_04_dull_material,
                                    iSS_AO_05_material
                                ]
                                Model {
                                    id: node20_P4_Truss
                                    x: 3.01558
                                    y: 0.00977945
                                    z: -0.0341015
                                    rotation: Qt.quaternion(0.866025, 0.5, 0, 0)
                                    source: "meshes/node20_P4_Truss.mesh"
                                    materials: [
                                        iSS_AO_05_material,
                                        iSS_02_dull_material,
                                        iSS_AO_03_material,
                                        iSS_02_dark___material,
                                        iSS_01_dull_material
                                    ]
                                    Model {
                                        id: node21_P5_Truss
                                        x: 8.1391
                                        y: -0.00870466
                                        z: -1.23359
                                        source: "meshes/node21_P5_Truss.mesh"
                                        materials: [
                                            iSS_01_dull_material,
                                            iSS_AO_06_material,
                                            iSS_02_dull_material,
                                            iSS_02_dark___material
                                        ]
                                        Model {
                                            id: node08_P6_Truss
                                            x: 1.44467
                                            z: 0.84866
                                            source: "meshes/node08_P6_Truss.mesh"
                                            materials: [
                                                iSS_02_dull_material,
                                                iSS_AO_03_material,
                                                iSS_01_dull_material,
                                                iSS_02_dark___material
                                            ]
                                            Model {
                                                id: node08_P6_Truss_01
                                                x: 13.4921
                                                y: -0.681999
                                                z: 1.53416
                                                source: "meshes/node08_P6_Truss_01.mesh"
                                                materials: [
                                                    iSS_02_dull_material,
                                                    shiny_panel_material,
                                                    iSS_02_dark___material
                                                ]
                                            }
                                            Model {
                                                id: node08_P6_Truss_02
                                                x: 13.4921
                                                y: 0.683402
                                                z: -1.52167
                                                source: "meshes/node08_P6_Truss_02.mesh"
                                                materials: [
                                                    iSS_02_dull_material,
                                                    shiny_panel_material,
                                                    iSS_02_dark___material
                                                ]
                                            }
                                        }
                                        Model {
                                            id: node20_P4_Truss_01
                                            x: -0.487251
                                            y: -0.680741
                                            z: 2.39077
                                            source: "meshes/node20_P4_Truss_01.mesh"
                                            materials: [
                                                iSS_02_dull_material,
                                                shiny_panel_material,
                                                iSS_02_dark___material
                                            ]
                                        }
                                        Model {
                                            id: node20_P4_Truss_02
                                            x: -0.487255
                                            y: 0.68466
                                            z: -0.665064
                                            source: "meshes/node20_P4_Truss_02.mesh"
                                            materials: [
                                                iSS_02_dull_material,
                                                shiny_panel_material,
                                                iSS_02_dark___material
                                            ]
                                        }
                                    }
                                }
                                Model {
                                    id: node35_Express_Logistics_Carrier__ELC__1
                                    x: 1.05663
                                    y: -5.07413
                                    z: -0.0445876
                                    source: "meshes/node35_Express_Logistics_Carrier__ELC__1.mesh"
                                    materials: [
                                        iSS_AO_07_material,
                                        iSS_03_dull_material,
                                        iSS_04_dull_material,
                                        iSS_01_dull_material
                                    ]
                                }
                                Model {
                                    id: node43_Express_Logistics_Carrier__ELC__3
                                    x: 1.84266
                                    y: 5.05736
                                    z: -0.0723591
                                    source: "meshes/node43_Express_Logistics_Carrier__ELC__3.mesh"
                                    materials: [
                                        iSS_AO_08_material,
                                        iSS_01_dull_material,
                                        iSS_04_dull_material,
                                        iSS_03_dull_material
                                    ]
                                }
                            }
                        }
                    }
                    Model {
                        id: node26_Harmony_Node_2
                        z: 9.16395
                        source: "meshes/node26_Harmony_Node_2.mesh"
                        materials: [
                            _material,
                            iSS_01_shiny_n_material,
                            iSS_AO_01_material,
                            iSS_01_dull_material
                        ]
                        Model {
                            id: node04__PMA__Pressurized_Mating_Adapter_2
                            z: 6.96839
                            source: "meshes/node04__PMA__Pressurized_Mating_Adapter_2.mesh"
                            materials: [
                                iSS_01_dull_material,
                                iSS_AO_02_material,
                                iSS_AO_01_material
                            ]
                        }
                        Model {
                            id: node27_Columbus_Space_Laboratory
                            x: -4.17182
                            y: -0.000374058
                            z: 4.82954
                            source: "meshes/node27_Columbus_Space_Laboratory.mesh"
                            materials: [
                                _material,
                                iSS_02_dull_material,
                                iSS_01_dull_material,
                                iSS_01_shiny_n_material,
                                iSS_AO_01_material,
                                iSS_AO_06_material
                            ]
                            Model {
                                id: node45_RapidScat
                                x: -5.90068
                                y: -1.33363
                                z: -4.00543e-05
                                source: "meshes/node45_RapidScat.mesh"
                                materials: [
                                    iSS_04_dull_material,
                                    iSS_AO_07_material,
                                    iSS_03_dull_material,
                                    foil_silver_material
                                ]
                                Model {
                                    id: rapidScat_dish
                                    x: -0.730886
                                    y: -0.825916
                                    z: -0.219193
                                    source: "meshes/rapidScat_dish.mesh"
                                    materials: [
                                        iSS_04_dull_material
                                    ]
                                }
                            }
                        }
                        Model {
                            id: node30_Kibo_Space_Laboratory__PSM__Pressurized_Module
                            x: 7.48092
                            y: -0.000371476
                            z: 4.82954
                            source: "meshes/node30_Kibo_Space_Laboratory__PSM__Pressurized_Module.mesh"
                            materials: [
                                _material,
                                iSS_03_dull_material,
                                iSS_01_dull_material,
                                iSS_AO_01_material,
                                iSS_03_shiny_n_material,
                                iSS_AO_04_material
                            ]
                            Model {
                                id: node28_Kibo_Space_Laboratory__PSM__Pressurized_Stowage_Module
                                x: 2.82219
                                y: 4.03747
                                z: -0.00196075
                                source: "meshes/node28_Kibo_Space_Laboratory__PSM__Pressurized_Stowage_Module.mesh"
                                materials: [
                                    _material,
                                    iSS_01_dull_material,
                                    iSS_03_shiny_n_material,
                                    iSS_AO_01_material,
                                    iSS_AO_04_material,
                                    iSS_03_dull_material
                                ]
                            }
                            Model {
                                id: node31_Robotic_Arm
                                x: 5.13823
                                y: 0.876178
                                z: 1.68101
                                source: "meshes/node31_Robotic_Arm.mesh"
                                materials: [
                                    iSS_04_dull_material
                                ]
                                Model {
                                    id: node31_Robotic_Arm_001
                                    x: 0.756553
                                    y: 0.296101
                                    z: -0.326674
                                    source: "meshes/node31_Robotic_Arm_001.mesh"
                                    materials: [
                                        iSS_04_dull_material
                                    ]
                                    Model {
                                        id: node31_Robotic_Arm_002
                                        x: -2.86102e-05
                                        y: 0.408472
                                        z: 0.529161
                                        source: "meshes/node31_Robotic_Arm_002.mesh"
                                        materials: [
                                            iSS_04_dull_material
                                        ]
                                        Model {
                                            id: node31_Robotic_Arm_003
                                            x: 4.06645
                                            z: -0.536636
                                            source: "meshes/node31_Robotic_Arm_003.mesh"
                                            materials: [
                                                iSS_04_dull_material
                                            ]
                                            Model {
                                                id: node31_Robotic_Arm_004
                                                x: 4.07438
                                                z: -0.520132
                                                source: "meshes/node31_Robotic_Arm_004.mesh"
                                                materials: [
                                                    iSS_04_dull_material
                                                ]
                                                Model {
                                                    id: node31_Robotic_Arm_005
                                                    x: 0.443251
                                                    y: 0.515401
                                                    z: 0.0115967
                                                    source: "meshes/node31_Robotic_Arm_005.mesh"
                                                    materials: [
                                                        iSS_04_dull_material
                                                    ]
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            Model {
                                id: node33_Kibo_Space_Laboratory_Exposed_Platform
                                x: 6.0541
                                y: -1.5613
                                z: -0.0877132
                                source: "meshes/node33_Kibo_Space_Laboratory_Exposed_Platform.mesh"
                                materials: [
                                    iSS_03_dull_material,
                                    iSS_04_dull_material,
                                    iSS_AO_07_material,
                                    iSS_01_dull_material
                                ]
                            }
                            Model {
                                id: cATS
                                x: 10.493
                                y: -1.94573
                                z: -1.07675
                                source: "meshes/cATS.mesh"
                                materials: [
                                    iSS_03_dull_material,
                                    iSS_01_dull_material
                                ]
                            }
                        }
                    }
                    Model {
                        id: destiny_Window_Cover
                        x: -0.000216407
                        y: -2.32269
                        z: 4.78135
                        source: "meshes/destiny_Window_Cover.mesh"
                        materials: [
                            iSS_01_dull_material
                        ]
                    }
                }
                Model {
                    id: node12_Quest_Airlock
                    x: -3.69454
                    y: -0.367075
                    source: "meshes/node12_Quest_Airlock.mesh"
                    materials: [
                        _material,
                        iSS_03_dull_material,
                        iSS_AO_04_material,
                        iSS_01_dull_material,
                        iSS_03_shiny_n_material,
                        iSS_01_shiny_n_material,
                        iSS_AO_01_material
                    ]
                    Model {
                        id: node18_ESP_External_Stowage_Platform_2
                        x: 0.642066
                        y: -0.00402007
                        z: 2.42742
                        source: "meshes/node18_ESP_External_Stowage_Platform_2.mesh"
                        materials: [
                            iSS_04_dull_material,
                            iSS_01_dull_material,
                            iSS_01_shiny_n_material,
                            iSS_AO_07_material
                        ]
                    }
                }
                Model {
                    id: node37_Tranquility_Node_3
                    x: 2.23406
                    y: -0.000385444
                    z: 3.1302e-05
                    source: "meshes/node37_Tranquility_Node_3.mesh"
                    materials: [
                        _material,
                        iSS_01_shiny_n_material,
                        iSS_AO_01_material,
                        iSS_01_dull_material
                    ]
                    Model {
                        id: node07__PMA__Pressurized_Mating_Adapter_3
                        x: 6.968
                        y: 0.000385366
                        z: -3.13802e-05
                        source: "meshes/node07__PMA__Pressurized_Mating_Adapter_3.mesh"
                        materials: [
                            iSS_01_dull_material,
                            iSS_AO_02_material,
                            iSS_AO_01_material
                        ]
                    }
                    Model {
                        id: node38_Cupola
                        x: 4.82758
                        y: -2.6225
                        z: 0.000315157
                        source: "meshes/node38_Cupola.mesh"
                        materials: [
                            iSS_04_dull_material,
                            iSS_01_dull_material,
                            iSS_AO_01_material
                        ]
                        Model {
                            id: node38_Cupola_001
                            x: 1.19997
                            y: -0.357734
                            z: -0.0117708
                            source: "meshes/node38_Cupola_001.mesh"
                            materials: [
                                iSS_04_dull_material
                            ]
                        }
                        Model {
                            id: node38_Cupola_002
                            x: 0.584978
                            y: -0.358087
                            z: 1.04811
                            source: "meshes/node38_Cupola_002.mesh"
                            materials: [
                                iSS_04_dull_material
                            ]
                        }
                        Model {
                            id: node38_Cupola_003
                            x: -0.574697
                            y: -0.357805
                            z: 1.05386
                            source: "meshes/node38_Cupola_003.mesh"
                            materials: [
                                iSS_04_dull_material
                            ]
                        }
                        Model {
                            id: node38_Cupola_004
                            x: -1.19998
                            y: -0.357651
                            z: 5.96943e-06
                            source: "meshes/node38_Cupola_004.mesh"
                            materials: [
                                iSS_04_dull_material
                            ]
                        }
                        Model {
                            id: node38_Cupola_005
                            x: -0.584963
                            y: -0.357934
                            z: -1.04808
                            source: "meshes/node38_Cupola_005.mesh"
                            materials: [
                                iSS_04_dull_material
                            ]
                        }
                        Model {
                            id: node38_Cupola_006
                            x: 0.615178
                            y: -0.357933
                            z: -1.03063
                            source: "meshes/node38_Cupola_006.mesh"
                            materials: [
                                iSS_04_dull_material
                            ]
                        }
                        Model {
                            id: node38_Cupola_007
                            x: -0.617941
                            y: -1.04861
                            z: 0.302734
                            source: "meshes/node38_Cupola_007.mesh"
                            materials: [
                                iSS_04_dull_material
                            ]
                        }
                        Model {
                            id: iSS
                            y: -0.0229638
                            source: "meshes/iSS.mesh"
                            materials: [
                                _material
                            ]
                        }
                    }
                }
                Model {
                    id: node40_Leonardo_Raffaello_Perm_Multi_Purpose_Logistics_Module__PMM_
                    x: -0.000315367
                    y: -2.19143
                    source: "meshes/node40_Leonardo_Raffaello_Perm_Multi_Purpose_Logistics_Module__PMM_.mesh"
                    materials: [
                        _material,
                        iSS_01_dull_material,
                        iSS_01_shiny_n_material,
                        iSS_AO_01_material,
                        iSS_AO_06_material
                    ]
                }
            }
        }
        Model {
            id: panel_01_p
            x: 1.68683
            y: 0.84555
            z: -11.087
            source: "meshes/panel_01_p.mesh"
            materials: [
                iSS_02_dull_material,
                shiny_panel_material
            ]
        }
        Model {
            id: hinge_02_p
            x: 2.17269
            y: 0.796646
            z: -12.5077
            source: "meshes/hinge_02_p.mesh"
            materials: [
                iSS_02_dull_material
            ]
        }
        Model {
            id: panel_03_p
            x: 2.93697
            y: 0.796646
            z: -9.62972
            source: "meshes/panel_03_p.mesh"
            materials: [
                iSS_02_dull_material,
                shiny_panel_material
            ]
        }
        Model {
            id: hinge_04_p
            x: 3.90954
            y: 0.79663
            z: -12.4982
            source: "meshes/hinge_04_p.mesh"
            materials: [
                iSS_02_dull_material
            ]
        }
        Model {
            id: hinge_01_p
            x: 1.68683
            y: 0.796638
            z: -11.087
            source: "meshes/hinge_01_p.mesh"
            materials: [
                iSS_02_dull_material
            ]
        }
        Model {
            id: panel_02_p
            x: 2.16748
            y: 0.796639
            z: -9.66369
            source: "meshes/panel_02_p.mesh"
            materials: [
                iSS_02_dull_material,
                shiny_panel_material
            ]
        }
        Model {
            id: hinge_03_p
            x: 2.92834
            y: 0.796653
            z: -12.5423
            source: "meshes/hinge_03_p.mesh"
            materials: [
                iSS_02_dull_material
            ]
        }
        Model {
            id: panel_04_p
            x: 3.90564
            y: 0.796651
            z: -9.6758
            source: "meshes/panel_04_p.mesh"
            materials: [
                iSS_02_dull_material,
                shiny_panel_material
            ]
        }
        Model {
            id: panel_01_s
            x: -1.68684
            y: 0.845551
            z: -11.087
            source: "meshes/panel_01_s.mesh"
            materials: [
                iSS_02_dull_material,
                shiny_panel_material
            ]
        }
        Model {
            id: hinge_02_s
            x: -2.17269
            y: 0.796646
            z: -12.5077
            source: "meshes/hinge_02_s.mesh"
            materials: [
                iSS_02_dull_material
            ]
        }
        Model {
            id: panel_03_s
            x: -2.93697
            y: 0.796652
            z: -9.62972
            source: "meshes/panel_03_s.mesh"
            materials: [
                shiny_panel_material,
                iSS_02_dull_material
            ]
        }
        Model {
            id: hinge_04_s
            x: -3.90954
            y: 0.796646
            z: -12.4982
            source: "meshes/hinge_04_s.mesh"
            materials: [
                iSS_02_dull_material
            ]
        }
        Model {
            id: hinge_03_s
            x: -2.92835
            y: 0.796643
            z: -12.5423
            source: "meshes/hinge_03_s.mesh"
            materials: [
                iSS_02_dull_material
            ]
        }
        Model {
            id: panel_04_s
            x: -3.90564
            y: 0.796634
            z: -9.6758
            source: "meshes/panel_04_s.mesh"
            materials: [
                shiny_panel_material,
                iSS_02_dull_material
            ]
        }
        Model {
            id: panel_02_s
            x: -2.16748
            y: 0.79664
            z: -9.66368
            source: "meshes/panel_02_s.mesh"
            materials: [
                shiny_panel_material,
                iSS_02_dull_material
            ]
        }
        Model {
            id: hinge_01_s
            x: -1.68684
            y: 0.796638
            z: -11.087
            source: "meshes/hinge_01_s.mesh"
            materials: [
                iSS_02_dull_material
            ]
        }
        Model {
            id: oCO3
            x: 15.5322
            y: -2.12307
            z: 19.8971
            source: "meshes/oCO3.mesh"
            materials: [
                white_material,
                base_metal_material,
                iSS_01_dull_material,
                iSS_03_dull_001_material
            ]
        }
        Model {
            id: eCOStress
            x: 18.9139
            y: -1.95714
            z: 16.1068
            source: "meshes/eCOStress.mesh"
            materials: [
                ecostress_material,
                plastic_black_material,
                ecostressWhite_material,
                iSS_03_dull_002_material,
                ecostress_metal_material,
                iSS_01_dull_material,
                ecostress_dexter_material
            ]
        }
    }
}
