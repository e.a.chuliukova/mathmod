import QtQuick
import QtQuick3D

Node {
    // Materials
    DefaultMaterial {
        id: defaultMaterial_material
        diffuseColor: "#ff999999"
    }
    DefaultMaterial {
        id: _94_material
        diffuseColor: "#ff999999"
    }
    // end of Materials

    Node {
        id: pyr_obj
        Model {
            id: defaultobject
            source: "meshes/defaultobject.mesh"
            materials: [
                _94_material
            ]
        }
    }
}
