import QtQuick
import QtQml
import QtQuick.Layouts
import QtCharts
import QtQuick.Controls



Window {
    id: window
    width: 700
    height: 700
    visible: true
    property bool impulse: false
    ValueAxis {
        id: valueAxisX
        min: -5
        max: 5
    }
    ValueAxis {
        id: valueAxisY
        min: -5
        max: 5
    }
    RowLayout
    {
        id:row_1
        anchors.fill: parent
        spacing: 2
    ChartView
        {
           id:chart_1
           legend.visible: false
           Layout.fillHeight: true
           Layout.minimumWidth: parent.width
           Rectangle {
               id:main_dialog
               visible: false
               width: 300
               height: 300
               anchors.centerIn: parent
               ColumnLayout {
                   anchors.fill: parent

                   GridLayout {
                       columns: 2
                       Text {
                           id: text1
                           text: qsTr("X: ")
                           font.pixelSize: 20
                       }

                       TextEdit {
                           id: textEdit_X
                           text: qsTr("0")
                           font.pixelSize: 12
                           Layout.preferredHeight: 20
                           Layout.preferredWidth: 80

                       }

                       Text {
                           id: text2
                           text: qsTr("Y: ")
                           font.pixelSize: 20
                       }

                       TextEdit {
                           id: textEdit_Y
                           text: qsTr("0")
                           font.pixelSize: 12
                           Layout.preferredHeight: 20
                           Layout.preferredWidth: 80
                       }
                       Text {
                           id: text7
                           text: qsTr("alpha: ")
                           font.pixelSize: 20
                       }

                       TextEdit {
                           id: textEdit_alpha
                           text: qsTr("1")
                           font.pixelSize: 12
                           Layout.preferredHeight: 20
                           Layout.preferredWidth: 80
                       }

                       Text {
                           id: text5
                           text: qsTr("Значение: ")
                           font.pixelSize: 20
                       }

                       TextEdit {
                           id: textEdit_magn
                           text: qsTr("1")
                           font.pixelSize: 12
                           Layout.preferredHeight: 20
                           Layout.preferredWidth: 80
                       }

                   }

                   RowLayout {
                       Button {
                           id: button
                           text: qsTr("Поехали!")
                           Layout.preferredHeight: 40
                           Layout.preferredWidth: 90
                           onClicked:
                           {
                               window.impulse = true
                               data1.uX = Number(textEdit_X.text)*Number(textEdit_magn.text)
                               data1.uY = Number(textEdit_Y.text)*Number(textEdit_magn.text)
                               data1.alfa = Number(textEdit_alpha.text)
                               main_timer.start()
                               main_dialog.visible = false
                           }
                       }

                    id: data1
                    property real x_obj : 0.5
                    property real y_obj : 0.2
                    property real alfa : 0
                    property real t : 0
                    property real dx : 0
                    property real dy : 11
                    property real lambda : 0.0004
                    property real uX : 0
                    property real uY : 0
                    anchors.fill:parent
                    function pos_update()
                    {
                        x_obj = x_obj + lambda*dx
                        y_obj = y_obj + lambda*dy
                        dx = (dx + uX - lambda*39.86*x_obj/Math.pow(Math.sqrt(Math.pow(x_obj, 2)+Math.pow(y_obj, 2)), 3))+lambda*alfa*(x_obj/(Math.sqrt(Math.pow(x_obj, 2)+Math.pow(y_obj, 2))))
                        dy = (dy + uY - lambda*39.86*y_obj/Math.pow(Math.sqrt(Math.pow(x_obj, 2)+Math.pow(y_obj, 2)), 3))+lambda*alfa*(y_obj/(Math.sqrt(Math.pow(x_obj, 2)+Math.pow(y_obj, 2))))

                        return Qt.vector2d(x_obj, y_obj)
                    }


                      Timer
                      {
                          id : main_timer
                          property real counter: 0
                          interval: 5;
                          running: true;
                          repeat: true
                          onTriggered:
                          {
                              var temp = data1.pos_update()
                              data_1.clear()
                              data_1.append(temp.x, temp.y)
                              line_test.insert(main_timer.counter, temp.x, temp.y)
                              if (window.impulse)
                              {
                                  line_test_2.color = "black"
                                  line_test_2.append(temp.x, temp.y)
                              }
                              main_timer.counter++;
                          }
                      }
                                          }
                                      }

                                  }

           ScatterSeries
           {
               id:data_1
               color: "gray"
               markerSize: 10
               axisX: valueAxisX
               axisY: valueAxisY
           }
           ScatterSeries
           {
               id:data_central
               color: "blue"
               markerSize: 50
               XYPoint { x: 0; y: 0}
           }
           LineSeries
           {
               id:line_test
           }
           LineSeries
           {
               id:line_test_2

           }
           Button
                      {
                          id: but1
                          text: window.impulse ? "Стоп" : "Придать импульс"
                          checkable: true
                          checked: false
                          display: AbstractButton.TextOnly
                          anchors.bottom: parent.bottom
                          anchors.left: parent.left
                          onCheckedChanged:
                          {
                              if (window.impulse)
                              {
                                  data1.uX = 0
                                  data1.uY = 0
                                  data1.alfa = 0
                                  window.impulse = false
                              }
                              else
                              {
                                  main_dialog.visible = true
                                  main_timer.stop()
                                  data1.uX = 0
                                  data1.uY = 0
                                  data1.alfa = 0
                              }
                          }
                      }
}
}
}
